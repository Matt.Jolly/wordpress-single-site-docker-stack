#!/bin/bash
# Copyright (C) 2022 Matt Jolly

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# WordPress Docker stack configuration script.
# This script will use `sed` to substitute replacement variables to configure the URI
# that you want your service to run on. It will also configure the mariadb credentials.

function volume_helper () {
    if [ $( docker volume ls -q | grep $1-${SANITISED_DOMAIN} ) ]; then
        echo "Docker volume named $1-${SANITISED_DOMAIN} already exists."
        read -r -p "Do you want to delete the volume and start fresh? (y/N)" PURGE_VOLUME
        case "${PURGE_VOLUME}" in
            [yY][eE][sS]|[yY] ) docker volume rm $1-${SANITISED_DOMAIN} && docker volume create $1-${SANITISED_DOMAIN};;
            * ) ;;
        esac
    else 
        docker volume create $1-${SANITISED_DOMAIN}
    fi
}

echo "Please enter the fully qualified domain that you want to use for this WordPress instance:"
read DOMAIN
SANITISED_DOMAIN=${DOMAIN//./-}

printf "\n"
echo "Please enter the password for your mariadb 'root' user"
read -sr MARIADB_ROOT_PASSWORD
# Escape any special characters in the password
ESCAPED_MARIADB_ROOT_PASSWORD=$(sed -e 's/[$\/&]/\\&/g' <<< $MARIADB_ROOT_PASSWORD)
unset MARIADB_ROOT_PASSWORD

printf "\n"
echo "Please enter the password for your mariadb 'wordpress' user"
read -sr MARIADB_WORDPRESS_PASSWORD
ESCAPED_MARIADB_WORDPRESS_PASSWORD=$(sed -e 's/[$\/&]/\\&/g' <<< $MARIADB_WORDPRESS_PASSWORD)
unset MARIADB_WORDPRESS_PASSWORD

printf "\n"
echo "Configuring your Docker Compose file."
sed -i "s/«DOMAIN»/${DOMAIN}/g" docker-compose.yml
sed -i "s/«MARIADB_ROOT_PASSWORD»/${ESCAPED_MARIADB_ROOT_PASSWORD}/g" docker-compose.yml
sed -i "s/«MARIADB_WORDPRESS_PASSWORD»/${ESCAPED_MARIADB_WORDPRESS_PASSWORD}/g" docker-compose.yml 
sed -i "s/«SANITISED_DOMAIN»/${SANITISED_DOMAIN}/g" docker-compose.yml

unset ESCAPED_MARIADB_ROOT_PASSWORD 
unset ESCAPED_MARIADB_WORDPRESS_PASSWORD

echo "Creating docker volumes for WordPress persistent data"

volume_helper wordpress
volume_helper mariadb

echo "Done. You should be able to deploy the stack using:"
echo "'docker stack deploy -c docker-compose.yml wordpress'"
