# WordPress Single Site Docker Stack

This repo contains the docker-compose file for a WordPress Single Site docker stack deployment. It requires some (basic) configuration, and assumes that you are using [Traefik](https://traefik.io/). An easily-deployed Traefik stack can be found [here](https://gitlab.com/Matt.Jolly/traefik-grafana-prometheus-docker).


## Usage

For convenience, the included `configure-compose.sh` script may be run to configure the `docker-compose.yml`.

```bash
docker stack deploy -c docker-compose.yml wordpress
```

### Manual Configuration

Substitute `«DOMAIN»`, `«SANITISED_DOMAIN»`, `«MARIADB_ROOT_PASSWORD»`, and `«MARIADB_WORDPRESS_PASSWORD»` in `docker-compose.yml` to suit your environment. If you do not wish to use Traefik, you will need to expose port `80` on the container.

### Installation

Once your instance is running you will need to configure the WordPress instance using its installation wizard.

### Upgrades

WordPress updates need to be performed through the WordPress GUI - the docker image only contains the base system updates.

Just update the image version in your docker-compose file and redeploy the stack (or use [Shepherd](https://github.com/djmaze/shepherd) to automate):

```bash
docker pull wordpress:latest
docker stack rm wordpress
docker stack deploy -c docker-compose.yml wordpress
```